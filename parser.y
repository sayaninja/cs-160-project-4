%{
    #include <cstdlib>
    #include <cstdio>
    #include <iostream>
    #include "ast.hpp"
    
    #define YYDEBUG 1
    int yylex(void);
    void yyerror(const char *);
    
    extern ASTNode* astRoot;
%}

%error-verbose
/* List all your tokens here */

%token T_NUMBER T_ID T_INT
%token T_PLUS T_MINUS 
%token T_MULTIPLY T_DIVIDE 
%token T_LESS T_LESSOREQUAL 
%token T_EQUAL T_EQUALS 
%token T_AND T_OR T_NOT
%token T_PRINT T_RETURN 
%token T_IF T_ELSE T_WHILE 
%token T_OPENPAREN T_CLOSEPAREN 
%token T_OPENBRACK T_CLOSEBRACK 
%token T_EXTENDS T_NEW T_NONE
%token T_BOOL T_FALSE T_TRUE 
%token T_COLON T_DOT T_COMMA T_ARROW

/* Specify precedence here */

%left T_OR
%left T_AND
%left T_LESS T_LESSOREQUAL T_EQUALS
%left T_PLUS T_MINUS
%left T_MULTIPLY T_DIVIDE
%right T_NOT U_MINUS

/* Specify types for all nonterminals and necessary terminals here */

/* Non-terminals */
%type <program_ptr> Start 
%type <class_list_ptr> Classes 
%type <class_ptr> Class
%type <declaration_list_ptr> Members Declarations
%type <declaration_ptr> Member
%type <identifier_list_ptr> Declaration
%type <method_list_ptr> Methods
%type <method_ptr> Method
%type <expression_ptr> Expression 
%type <type_ptr> Type ReturnType
%type <methodbody_ptr> Body
%type <statement_list_ptr> Statements Block
%type <statement_ptr> Statement
%type <returnstatement_ptr> Return
%type <assignment_ptr> Assignment
%type <ifelse_ptr> IfElse
%type <while_ptr> While
%type <print_ptr> Print
%type <methodcall_ptr> MethodCall
%type <parameter_list_ptr> Parameters ParametersP
%type <parameter_ptr> Parameter
%type <expression_list_ptr> Arguments Argument

/* Terminals */
%type <base_char_ptr> T_ID
%type <booleantype_ptr> T_BOOL
%type <integertype_ptr> T_INT
%type <base_int> T_NUMBER T_FALSE T_TRUE







 
%%

/* Grammar  */

Start       : Classes  { $$ = new ProgramNode($1); astRoot = $$; }
            ;

Classes     : Classes Class { $$ = $1; $$->push_back($2); }
            | Class { $$ = new std::list<ClassNode*>(); $$->push_back($1); }
            ;
Class       : T_ID T_EXTENDS T_ID T_OPENBRACK Members Methods T_CLOSEBRACK {$$ = new ClassNode(new IdentifierNode($1),new IdentifierNode($3),$5,$6);}
            | T_ID T_OPENBRACK Members Methods T_CLOSEBRACK {$$ = new ClassNode(new IdentifierNode($1),NULL,$3, $4);}
            ;  

Members     : Members Member { $$ = $1; $$->push_back($2); }
            | { $$ = new std::list<DeclarationNode*>(); }
            ;

Member      : Type T_ID {$$ = new DeclarationNode($1, new std::list <IdentifierNode*>(1, new IdentifierNode($2))); }
            ;

Type        : T_INT  { $$ = new IntegerTypeNode(); }
            | T_BOOL { $$ = new BooleanTypeNode(); }
            | T_ID   { $$ = new ObjectTypeNode(new IdentifierNode($1)); }
            ;     

ReturnType  : Type { $$ = $1; astRoot = $$; }
            | T_NONE { $$ = new NoneNode(); }
            ;     

Methods     : Method Methods { $$ = $2; $$->push_front($1); }
            | { $$ = new std::list<MethodNode*>(); }
            ;

Method      : T_ID T_OPENPAREN Parameters T_CLOSEPAREN T_ARROW ReturnType T_OPENBRACK Body T_CLOSEBRACK { $$ = new MethodNode(new IdentifierNode($1), $3, $6, $8);  }
            ;

Parameters  : ParametersP { $$ = $1; }
            | { $$ = NULL; }
            ;

ParametersP : ParametersP T_COMMA Parameter {$$ = $1; $$->push_back($3);}
            | Parameter {$$ = new std::list<ParameterNode*>(); $$->push_back($1);}
            ;

Parameter   : T_ID T_COLON Type { $$ = new ParameterNode($3, new IdentifierNode($1));}
            ;

Body        : Declarations Statements Return { $$ = new MethodBodyNode($1, $2, $3);  }
            ;


Declarations: Declarations Type Declaration {$$ = $1; $$->push_back(new DeclarationNode($2,$3));}
            | {$$ = new std::list <DeclarationNode*>();}
            ;

Declaration :  Declaration T_COMMA T_ID {$$ = $1; $$->push_back( new IdentifierNode($3));}
            | T_ID {$$ = new std::list <IdentifierNode*>(); $$->push_back(new IdentifierNode($1));}

Statements  : Statement Statements { $$ = $2; $$->push_front($1); }
            | { $$ = new std::list<StatementNode*>(); }
            ;

Return      : T_RETURN Expression { $$ = new ReturnStatementNode($2); }
            | { $$ = NULL; }
            ;

Statement   : Assignment { $$ = $1; }
            | MethodCall { $$ = new CallNode($1); }
            | IfElse { $$ = $1; }
            | While { $$ = $1; }
            | Print { $$ = $1; }
            ;

Assignment  : T_ID T_EQUAL Expression { $$ = new AssignmentNode(new IdentifierNode($1), NULL, $3); }
            | T_ID T_DOT T_ID T_EQUAL Expression { $$ = new AssignmentNode(new IdentifierNode($1), new IdentifierNode($3), $5); }
            ;

MethodCall  : T_ID T_OPENPAREN Arguments T_CLOSEPAREN { $$ = new MethodCallNode(new IdentifierNode($1), NULL, $3); }
            | T_ID T_DOT T_ID T_OPENPAREN Arguments T_CLOSEPAREN { $$ = new MethodCallNode(new IdentifierNode($1), new IdentifierNode($3), $5); }

IfElse      : T_IF Expression T_OPENBRACK Block T_CLOSEBRACK { $$ = new IfElseNode($2, $4, NULL); }
            | T_IF Expression T_OPENBRACK Block T_CLOSEBRACK T_ELSE T_OPENBRACK Block T_CLOSEBRACK { $$ = new IfElseNode($2, $4, $8); } 
            ;

While       : T_WHILE Expression T_OPENBRACK Block T_CLOSEBRACK { $$ = new WhileNode($2, $4); }
            ;

Block       : Block Statement { $$ = $1; $$->push_back($2); }
            | Statement { $$ = new std::list<StatementNode*>(); $$->push_back($1); }
            ;

Print       : T_PRINT Expression { $$ = new PrintNode($2); }
            ;

Expression  : Expression T_PLUS Expression { $$ = new PlusNode($1, $3); }
            | Expression T_MINUS Expression { $$ = new MinusNode($1, $3); }
            | Expression T_MULTIPLY Expression { $$ = new TimesNode($1, $3); }
            | Expression T_DIVIDE Expression { $$ = new DivideNode($1, $3); }
            | Expression T_LESS Expression { $$ = new LessNode($1, $3); }
            | Expression T_LESSOREQUAL Expression { $$ = new LessEqualNode($1, $3); }
            | Expression T_EQUALS Expression { $$ = new EqualNode($1, $3); }
            | Expression T_AND Expression { $$ = new AndNode($1, $3); }
            | Expression T_OR Expression { $$ = new OrNode($1, $3); }
            | T_NOT Expression { $$ = new NotNode($2); }
            | T_MINUS Expression %prec U_MINUS { $$ = new NegationNode($2); }
            | T_ID { $$ = new VariableNode(new IdentifierNode($1)); }
            | T_ID T_DOT T_ID { $$ = new MemberAccessNode(new IdentifierNode($1), new IdentifierNode($3)); }       
            | MethodCall { $$ = $1; }                           
            | T_OPENPAREN Expression T_CLOSEPAREN { $$ = $2; }
            | T_NUMBER { $$ = new IntegerLiteralNode(new IntegerNode($1)); }
            | T_TRUE { $$ = new BooleanLiteralNode(new IntegerNode($1)); }
            | T_FALSE { $$ = new BooleanLiteralNode(new IntegerNode($1)); }
            | T_NEW T_ID { $$ = new NewNode(new IdentifierNode($2), NULL); }
            | T_NEW T_ID T_OPENPAREN Arguments T_CLOSEPAREN { $$ = new NewNode(new IdentifierNode($2), $4); }
            ;


Arguments   : Argument { $$ = $1; }
            | { $$ = new std::list<ExpressionNode*>(); }
            ;

Argument    : Argument T_COMMA Expression { $$ = $1; $$->push_back($3); }
            | Expression { $$ = new std::list<ExpressionNode*>(); $$->push_back($1); }
            ;


%%

extern int yylineno;

void yyerror(const char *s) {
  fprintf(stderr, "%s at line %d\n", s, yylineno);
  exit(1);
}
